function reverseString(inputString) {
  // explode string into an array
  let splitinput = inputString.split("");
  var output = "";
  // loop throught charaters of input string in reverse
  var length = splitinput.length;
  for (i = 0; i < length; i++) {
    // add charaters to new variable
    output = splitinput[i].concat(output);
  }
  // join array into string
  // console.log(output);
  return output;
}

module.exports = reverseString;
