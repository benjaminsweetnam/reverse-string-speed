function reverseString(inputString) {
  return inputString.split("").reduce((acc, next) => next + acc);
}

module.exports = reverseString;
