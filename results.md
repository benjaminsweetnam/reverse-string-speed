
## For Results ##  
| Run# | Results |
|:-|:-|      
run 1 |0.238127ms
run 2 |0.026749ms
run 3 |0.005251ms

## Map Results ##  
| Run# | Results |
|:-|:-|
run 1 |0.490944ms  
run 2 |0.024812ms
run 3 |0.011125ms

## Natural Results ##
| Run# | Results |
|:-|:-|
run 1 | 0.109997ms
run 2 | 0.007414ms
run 3 | 0.005908ms

## Reduce Results ##
| Run# | Results |
|:-|:-|
run 1 | 0.097044ms
run 2 | 0.00928ms
run 3 | 0.002134ms

