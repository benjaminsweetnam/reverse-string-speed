const reverseStringFor = require("./string-reverse-for");
const reverseStringMap = require("./string-reverse-map");
const reverseStringReduce = require("./string-reverse-reduce");
const reverseStringNatural = require("./string-reverse-natural");

var output = "";
var start = null;

output += "\n## For Results ##  \n";
output += "| Run# | Results |\n";
output += "|:-|:-|      \n";
start = process.hrtime();
reverseStringFor("testing string");
var for1 = process.hrtime(start)[1] / 1e6;
output += "run 1 |" + for1 + "ms\n";

start = process.hrtime();
reverseStringFor("another string to test");
var for2 = process.hrtime(start)[1] / 1e6;
output += "run 2 |" + for2 + "ms\n";

start = process.hrtime();
reverseStringFor("this is a totally different string");
var for3 = process.hrtime(start)[1] / 1e6;
output += "run 3 |" + for3 + "ms\n";

output += "\n## Map Results ##  \n";
output += "| Run# | Results |\n";
output += "|:-|:-|\n";
start = process.hrtime();
reverseStringMap("testing string");
var map1 = process.hrtime(start)[1] / 1e6;
output += "run 1 |" + map1 + "ms  \n";

start = process.hrtime();
reverseStringMap("another string to test");
var map2 = process.hrtime(start)[1] / 1e6;
output += "run 2 |" + map2 + "ms\n";

start = process.hrtime();
reverseStringMap("this is a totally different string");
var map3 = process.hrtime(start)[1] / 1e6;
output += "run 3 |" + map3 + "ms\n";

output += "\n## Natural Results ##\n";
output += "| Run# | Results |\n";
output += "|:-|:-|\n";
start = process.hrtime();
reverseStringNatural("testing string");
var nat1 = process.hrtime(start)[1] / 1e6;
output += "run 1 | " + nat1 + "ms\n";

start = process.hrtime();
reverseStringNatural("testing string");
var nat2 = process.hrtime(start)[1] / 1e6;
output += "run 2 | " + nat2 + "ms\n";

start = process.hrtime();
reverseStringNatural("testing string");
var nat3 = process.hrtime(start)[1] / 1e6;
output += "run 3 | " + nat3 + "ms\n";

output += "\n## Reduce Results ##\n";
output += "| Run# | Results |\n";
output += "|:-|:-|\n";
start = process.hrtime();
reverseStringReduce("testing string");
var red1 = process.hrtime(start)[1] / 1e6;
output += "run 1 | " + red1 + "ms\n";

start = process.hrtime();
reverseStringReduce("another string to test");
var red2 = process.hrtime(start)[1] / 1e6;
output += "run 2 | " + red2 + "ms\n";

start = process.hrtime();
reverseStringReduce("this is a totally different string");
var red3 = process.hrtime(start)[1] / 1e6;
output += "run 3 | " + red3 + "ms\n";

console.log(output);
