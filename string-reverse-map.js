function reverseString(inputString) {
  return inputString
    .split("")
    .map((_char, index, arr) => arr[arr.length - index - 1])
    .join("");
}

module.exports = reverseString;
